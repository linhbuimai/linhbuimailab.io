Linh is an Engineer-to-be. Her current job title is "Analytics Engineer", before that, the title was "Data Engineer", "BI Engineer", "Data Analyst", "Business Analyst". Anyway, we do not care about working title here (though sometimes I wonder why people care so much about their "TITLE", which I should discuss in more detail in another post). Linh loves build things, but not have much patience so she often stumbles between different "hobbies". This website was bought for 7 years (which means each year she has to pay around $25-$30 for the domain `iamlinh.com`).

Initially, Linh wanted to build a professional website for career. Now, 7 years passed and nothing (much) happens. The website therefore is transformed to a "xàm" form of her life.

Thank you for being here, Linh.