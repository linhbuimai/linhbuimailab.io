+++
title = "hey, that's no way to say goodbye"
date = "2023-03-17"
draft = false
description = "Leonard Cohen and Julie Felix"
subtitle = ""
header_img = ""
short = false
toc = false
tags = ["life-soundtrack"]
categories = ["raw-thought"]
series = ["Life Soundtrack"]
+++

Nhạc chi mà tình hết mức. Tình nức nở trái tim Linh.

{{< youtube id="BzgUs3c9QHY" >}}