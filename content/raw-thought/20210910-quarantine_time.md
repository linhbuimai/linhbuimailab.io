---
title: To get the best out of quarantine time
date: "2021-09-10"
categories:
- raw-thought
tags:
- mics
toc: true
---

I was in Bangkok on this day, two years ago, just arrived after a chaos plane with Whiskey, drunk Indian men, and everything in between. This statement is based on the fact that 2020 is a leap year which means I have to plus one day more in my calculation. Back then in 2019, everything seems normal in the world, we could not be aware of the up-coming pandemic, the picture of quarantine for everyone in the whole world only happens in fantasy movie. I remember this time I was reading the book World War Z. It’s a nice story about how various countries deal with zoombie. Fast-forward to now, two more years and I’m officially experiencing covid-19 situation. The remaining of the world might experience covid pandemic earlier, from the very beginning of January 2020, but not us. Vietnam had taken strong actions, in our own special - sometimes decisive and peremptory- way, to prevent early breakout. Things had been good, but this situation did not last that long like everyone expected it should have been. Around May 2021, our thought-to-be-perfect policy has been failed, became inefficient and impotent in preventing the disease. Unfortunately, though not surprisingly, Saigon and other North provinces have been affected heavily. Living in Hanoi, it’s not until July that I have to stay at home to comply with government policy. I don’t want to reflect about the consequences of covid to my country, but navigate my focus on a smaller scale - which is myself - what I’ve learned about own mental state, both at stable and unstable time during quarantine. I also did some trials and fails, started tracking my own emotions in relation with the rise of the moon (which marks my monthly period lol). My objective is getting me familiar of myself, and create memorable experiences of that quarantine time. After all, “nếu không có gì để nhớ về, anh sợ lòng mình khô nứt nẻ” - extracted from Đen’s song.

> Nếu không có gì để nhớ về, anh sợ lòng mình khô nứt nẻ.

I’ve been in quarantine for almost 2 months, 16.7% time of a year, 1440 hours. People evaluated me as an introvert (though I try not to label myself as any type. Actually, for me, labeling my characteristics in order to understand my value is useless) then I should enjoy this time. But our human are social creature, trying to be alone, disconnect yourself with the world might destroy your brain functions. I had experienced this during the first month of my quarantine. So I would like to divide my 2 months quarantine into three parts:

- Over-worked and over-excited because of over-worked
- Stressed due to lack of appropriate rest and socially disconnected
- Self-aware and be patient to get more guts after a quite huge depression

So for the first part, I've been in charged of writing some glue stuff for my DED team while at the same time prepared materials for my Power BI online course. So in short, I was working more than 12 hours a day, from 9am to 2am of the next day. But I felt full-filled, or I guess I was supposed to. It was fun and challenging in getting myself familiar with the remote-work stuff: follow every online conversation, every mail to ensure not miss any information. Reading time during traveling between home and office had been gone. Day-dreaming time waiting for the elevator from 1st to 21st floor had been gone. Now I woke up, had morning routine, and went straight to my customized working desk one meter away from my bed, and three meters away from my toilet. In the beginning, I knew that this would not last long, but I did anyway. Why? Because I was stubborn and always ignore my own instinct lol. That's the simplest explanation for my first mistake.

In the second part, things started getting worse, like a reinforcing feedback loop. My system started collapsed. Due to lack of appropriate rest, and lack of appropriate nutrition, my normal functions had been broken, and my mental illness (the old friend) came back to me. I found myself mourning for the past - when I still had my gang in the office, drunk together and laughing at the silly face of each other, mourning for my unexpected future and regretting the hard-working and way-more-troublesome route (choose to learn programming from the start, and try to work in a fast-paced engineer team). There was time that I asked myself, why don't I choose to follow the easier path, to focus on my expertise. I haven't found the answer yet, until now, sadly. Anyway, my mental wall seemed broken down, I'd cried a lot, drank a lot, and complained a lot. In the side effects, show-off messages (to prove my worth) had been sent, angry and ungrateful attitude had been revealed. We could not expect anything nice and good of ourselves during depression time. When my past illness visited, it's tempted to hurt myself like the old days. The physical pain could help, I believe, to distract you from the mental pain - which is much worse. However, all the past self-reflection time came and rescue me. I was growth and learned count on my mental strength. So in short, no more physical hurt this time (which I found kinda proud of myself now) though still remains lots of trouble to untangle next.

In the next part (which is now), things are getting better, because you cannot be depressed all the time. We have up and down and what I try to do now is to mitigate the negative effect of my down-mood time (or worse, my depressed time), and lengthen the up-mood or not-that-stressed time. After all, my lifelong objective is being kind, and nice, and warm for those who need, and for myself (specially, I was my first and foremost and crucial customer of my service). Social distance time might get better or worse depending on how I perceive and use it. So I commit to myself optimize my quarantine time and commit to provide myself appropriate rest as well as nutrition, because "rubbish in, rubbish out". How can I expect a wonderful outcome while my input is just rubbish?

**The question is: How to optimize my time, to turn it into quality time, and get the best out of socially distant situation?**

### Have an appropriate rest and nice meal

Frankly, I've tried though not make noticeable progress. My bad habit would not simply run away overnight. But one point given to me, I've tried, lol.

### Have regular exercises

I know, I know. But I've not tried it yet. No, I'm not that determined. Maybe start small, with some stretching yoga lessons, 5-10 minutes each. Nah, I know it's best for my health and would solve tone of problems, plus a flatttt bellyyy. But nope. Maybe I will ride my bike in daily basis, at night, lol.

### Increase focus time

Follow Pomodoro technique, I use Forest app to track and monitor my focus time. When having 25 minutes deep focus, I will take a rest 5 minutes and plant a tree : )). My virtual forest is waiting for me. I use this app during working, cooking, studying, and practicing instrumental. Taking yourself into a deep focus mode (or flow) would optimize experience and increase your happiness at the end of the day - or of the life.

![forest app](/forest-app.png)

### Reduce online social network time

Yeah, I don't need a virtual friend to remind myself that their lives are nice and full of experiences while my life is struggled and boring. Talking with people helps me realize that our reality perception varies based on what our brain wants us to believe. Therefore, it's not necessary to show the whole world that I was desperate or in the peak of happiness. Sometimes, when in need of tribute my old memories, expressing love and gratitude to my friends, facebook and instagram might a big help. Other time, it's just a place to swap kitten pictures. 

### Reduce my expectation, and empathize people's complaints

Practice listening, and navigate my attention to people's problem and their underlying/hidden pain helps me become more patience. (Actually, three online power bi course did help a lot). When you're patient, nice things come: less negative energy but more sunshine and wind, and birds, and springs - anything beautiful in the world. I start listening and giving compliments instead of judging and criticizing when someone comes to me with complaints. I stop taking things personally (or at least I'm conscious of not to), instead, I write in my daily journal things learned from my mistake s.t I do not make the same one in the future. Huhm, there's some interesting sorts like:

> Không mua gạo lứt rong biển ở các shop online vì mình sẽ không ăn đâu.
> 
> Không mua mấy loại snack healthy ở các shop online vì mình cũng không ăn đâu.
> 
> (or more professional) Không xoá file linh tinh trừ khi có thể safe delete nó và make sure không ảnh hưởng đến người khác.
> 

It's nice to read this journal. 

I do not expect anyone get my situation. When I cannot get the work done, it's my fault, I do not expect someone can understand or not blame me. Huhm, I actually give up on counting on the sympathy of people. It's not a nice attitude, quite preposterous what I'm practicing. But isn't lower your expectation would lead you more fullfilling?

### Write more, read more, and leave a constant daily time doing nothing

Like now. I practice writing more, in official blog, not just my diary and try to navigate the content in subjective way. I'm back to my old habit "reading when having free time, when thinking about which movie to watch, I choose reading". There's some books I would like to write about, as the one which have huge influence in my life. Havng sometime doing nothing during a day would help me enjoy the little things, and be more conscious of the surrounding environment. I learned this mindset from meditation practice. But mediating is not my type. I just cannot sitting patiently and let my mind empty but focus on the breath. Nah, I need to see the sky, to smell the wind, to feel sweat in my face (oh it's still hot in Hanoi now), to touch the wall, to see the light, etc. Anyway, it's said that when you shut down a sense (in this case, your eyes), other senses would become stronger. Oki, next time I will close my eyes when feeling the wind blow through my hair.

### Track and discover my mood ranging pattern

I strongly believe in the effect of hormone to my emotions. It might be that during (or before) my monthly period, I am more sensitive and easily to overthinking, selfish, angry - to name a few. So foresee the mood trend would help me better aware and monitor my own emotions. 

Finally, I would like end this post with a song (that I'm listening now - actually spotify suggests this with the radio "True love will find you in the end"). 