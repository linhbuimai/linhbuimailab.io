---
title: Optimal way to manage your notes
date: "2020-06-17"
categories:
- raw-thought
tags: 
- note-taking
toc: true
---

### My Problems

Same period last year, I did wrote a note on how to optimize tag system in evernote such that I can manage my writings, useful bookmark, even control project documentation. This post hasn't finished yet due to the complicated tag system I designed. For a year, I've kept trying to use evernote, bear, physical notebooks, pen and papers, and other documentation management platform like trello, confluence, onedrive, google drive, etc. Now my notes and "knowledge-related" assets have growth tremendously such that I feel struggle whenever need find something. In short, there're several problems I have to face with my current note-system:
- My tagging/labeling system in evernote is too complicated and time-consuming, and frankly, not quite self-explained and logic (satisfis MECE principles at least). When start a new note, I have to think about topic, context, note type, etc. And as still struggle with slightly OCD, it feels like "not complete" if skipping some notes without any tag.
- I like writing in markdown and evernote is not my type.
- My notes is partition into several notebook platforms both electronic and physical which takes lot of effort to find something, even with appropriate tags and labels.
- My company prohibits using corporate website/software which means I cannot take note during works.

### Solutions

Then I came to [hackmd.io](hackmd.io). This is an online markdown note and luckily hasn't been banned in my company (They probably don't know about it). Hackmd supports online markdown writing, supports embedding other plaforms like youtube, gist, slideshare, UML diagrams, etc. But then I felt not quite satisfy with Hackmd only. Maybe the main reason is that I want a comprehensive notebook platform with the following features:
- Markdown style writing
- Simple interface (evernote is too complicated while bear is too simple)
- Integrated with Trello, or Chrome such that I can save "to-read" articles, bookmark links, quote books, etc.
- Free, or not too expensive
- Simple embedded images, youtube, gist
- Support code display for python, dax, etc.
- Support offline mode

I tested Trello, Confluence, and Evernote but Confluence is web-based, limited for free plan, and overkill for a personal knowledge base. Plus, I haven't created any connection between Confluence and Evernote. Then I tried another combination, replaced Confluence with Hackmd:
- [Trello](trello.com): bookmark important links (references, to read later, current reading, etc.), manage tasks and projects.
- [Hackmd.io](hackmd.io): write markdown-style notes, for speed note taking at office and at home.
- [Evernote](evernote.com): fast clipping contents in web, easily save article as to refer later, acts as an Inbox and Library for surfing contents.

Then how to connect the dots? Simply, there's several rules (again) when it comes to find anything in my knowledge base:
1. Trello is explicitly for bookmark-link with notes (what is content of this link), and tasks by projects.
2. Hackmd.io is for note-taking, includes summarize notes, thoughts, lessons learned, self-reflection, brainstorming.
3. Evernote is for reading reference (full content, not just a link like trello), with simple tag system arranged by topic.
4. Important topics will be linked between Hackmd and Evernote by link reference.

Yesterday, I realized a potential issue of hackmd. This is free platform (for personal use), and not famous at all lol. If I rely all my notes on just one platform, there's probability that one day the owner (6 developers) will disappear and my notes will be gone. I've already experienced this situation once (8tracks). Plus, the desktop app seems not functioning well like I expected. A simple solution is that I migrate all my notes from hackmd.io to gitlab, create a new md-notes repository in parallel with hackmd repository and set schedule to sync notes (manually lol). 

With the help of Gitlab, I can write in hackmd.io while working, paste the content to gitlab repo (which my company hasn't prohibited). Gitlab repository with VS Code provides a much simpler interface compares to Evernote's. Furthermore, I also set a strict rule to myself that raw thought, incomplete thought could be written by pen and paper but whatever its stage is, I need to transfer it to an electronic note in my md-notes repo (draft accepted, just flag/label it with "draft" keyword).

### What's next

This choice is just another test. It might work for me for now but I'm not sure if it could still working for a long time. With trial and test spirit, I'm still on the way to form the best note-taking system for my knowledge base. Why am I so obssessed with this note-taking things? Because I need to get my thoughts, worries, ideas out of my head as soon as possible, then share with somebody else, to key the knowledge to my long-term memory, and to build the "Linh's heritage". Who knows, we always long for building something from scratch, to feel better, to feel rewarded and progress in our meaningless lives. 