---
title: new ipad pro
date: "2023-02-15"
categories:
- raw-thought
tags:
- mics
- feel-good-stuff
--- 

I just bought a new Ipad this month in order to fullfill my longing on some fancy stuff. This post is writen on it as a first test for “How easy it is to write blog with git on my ipad” : )) Hi hi.

Hope gitlab can run CI successfully.

So the point is it cannot parse header of this post. “2023-02-15” now becomes “Jan 01, 0001”. Weird.

oki now I understand why. the application on my ipad which is “Working Copy” have a weird double quote `“”` <- this one. And it is different with the correct double quote for `string` type in post’s metadata. 

then how to fix? I don’t know yet but anyway I can write offline on my ipad then change this double quote on the online editor of gitlab. yay!

in short, writing blog on my ipad is possible, both offline and online.

next step: to find an editor app instead of using this git client.

bai! till next time.

p.s one small side thought: I love commit and push code with terminal instead of UI git client. but install an ssh client inside an “ipad termina” is quite “rách việc” to me.