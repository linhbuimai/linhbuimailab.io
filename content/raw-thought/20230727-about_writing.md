+++
title = "About writing"
date = "2023-07-27"
draft = true
description = "A self-reflection on my writing journey (hint: it's not that long as I thought it should be"
subtitle = ""
header_img = ""
short = false
toc = true
tags = ["obsidian", "note-taking", "writing"]
categories = ["raw-thought"]
series = ["Personal Writing"]
+++

Đặt tay vô bàn phím bắt đầu viết gì đó thấy rằng việc viết lách này thật khó. Không phải đơn thuần ngồi viết về một chủ đề là viết được. Vì rằng suy nghĩ trong đầu khi chưa rõ ràng sẽ chỉ viết ra một mớ bòng bong, người ta gọi là free writing. Kể cả rằng như vậy free writing không phải thích là viết được vì toi nghĩ đó là một sự nuông chiều bản thân. Nghĩ gì viết đó có thể dẫn đến hậu quả nghĩ gì nói đó - sự bất cẩn được gây dựng từ thói quen thường ngày. Toi đâu muốn là người nghĩ gì nói đó, phải không?
Đợt gần đây toi đọc essay của Bertrand Russel, đọc hoài mấy tuần mới xong được một bài. Toi đọc vì trước đó toi mở quyển sách “The sense of style” của Stenphen Pinker (Khánh bảo thích ông chú này, và quyển sách này), đọc thấy ổng bảo nên đọc "good writing" để tự rèn cho mình kĩ năng “viết tốt”. Ổng bảo rằng khi đọc thấy hay thì nhớ ngồi nghĩ coi vì sao nó hay, vì cách người ta dùng từ hay đảo câu hay làm cái gì toi không biết nhưng thấy nó hay vì sao.
Kết quả một bài essay đầu tiên ổng Bertrand Russel tôi đọc xong chưa rút được cái gì hết và phải đọc lại một lần nữa. Cho chắc. Chắc này là chắc hiểu, không phải như làm toán - làm lại thêm lần để kết quả chắc (chắc chắn đúng).

*Bữa tối nay toi uống chút rượu vang, thấy biêng biêng trong người.*

Sự bất lực của tôi về chuyện viết lách xảy đến khi toi không tìm được từ ngữ miêu tả cảnh đẹp đã chiêm ngưỡng. Cảnh đẹp đó ập đến với toi trong tình huống trớ trêu: khi toi ngồi máy bay nhòm cửa sổ. Cảnh đẹp đó, với ngôn ngữ mộc mạc (ngok nghek) của toi, được đặt tên là: Cảnh hoàng hôn trên biển (biển rất lớn, rất xịn) nhìn từ cửa sổ máy bay.

Sự bất ngờ của tôi về chuyện viết lách xảy đến khi toi đọc cuốn truyện gánh xiếc nào đó (tên tiếng anh, giờ tôi khum nhớ), và tôi cũng chỉ mới đọc hết chương đầu tiên của cuốn truyện đó. Tôi bất ngờ vì những gì câu chữ của nhà văn miêu tả khiến tôi (đâu đó) tin tưởng rằng mình đang đứng cùng một không gian với nhân vật nữ chính đó, nhìn ánh trăng chiếu trên làn da nhợt nhạt của cô bé đó khi cô ấy thử chiếc váy cưới của mẹ, nhìn cô ấy chạy ra ngoài vườn và tự sợ hãi khi cổ tưởng tượng bóng tối trước mắt đang cố chụp lấy cỏ. (tóm lại là: toi có thể như đang được thở cùng bầu không khí và tồn tại cùng không gian, thời gian với cô ấy).

Viết tiếng việt toi còn bất lực.

Khi buồn bực toi tìm đến blog của thần tượng. Bữa nọ toi kêu với bạn: thần tượng của toi đều hoặc già, hoặc tự sát (khi còn trẻ, và ở mãi trong cái tuổi trẻ, thời gian đó).
