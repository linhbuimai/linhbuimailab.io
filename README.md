## Hugo - Puppet theme getting started

**Why I love this theme?** It should pretty simple, because it's clean, and support TOC (table of contents).

### Post Templates

```
+++
title = "{{ replace .Name "-" " " | title }}"
date = {{ .Date }}
draft = true
description = ""
subtitle = ""
header_img = ""
short = false
toc = true
tags = []
categories = []
series = []
comment = true
+++
```

Rules:
- New post should put in `content/<sub_folder>`
- New post should follow template above, with appropriate tag. (Why? Because I love `tag`)

### What's next

Let's write. Linh.